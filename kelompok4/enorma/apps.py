from django.apps import AppConfig


class EnormaConfig(AppConfig):
    name = 'enorma'
