import functools
from datetime import datetime
from dateutil.relativedelta import relativedelta

# from django.conf import settings
from django.db import IntegrityError
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.views.decorators.http import require_http_methods
from django.template.loader import render_to_string

from weasyprint import HTML, CSS

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework_api_key.permissions import HasAPIKey
from .serializers import RiwayatNPPNSerializer, RiwayatNPPNSerializerAll, SPTTahunan1770Serializer

from .forms import VerifikasiSyarat
from enorma.models import DaftarNorma, RiwayatNPPN, MasterfileWajibPajak, SPTTahunan1770, AkunWajibPajak


# variabel
sekarang = datetime.now() + relativedelta(months=-6)
tahun_sekarang = sekarang.year
batas_lapor = datetime.strptime(f'{tahun_sekarang}-03-31', '%Y-%m-%d')


def wp_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.add_message(request, messages.WARNING, 'NPWP invalid.')
        else:
            messages.add_message(request, messages.WARNING, 'NPWP atau password salah.')
    else:
        form = AuthenticationForm()
    return render(request, 'enorma/login.html', context={"form":form})

@login_required
def home(request):
    return render(request, 'enorma/menu-home.html')

@login_required
def layanan(request):
    context = {
        'petunjuk': {
            'list_petunjuk': [
                'Menu layanan menampilkan jenis layanan perpajakan yang disediakan DJP dalam bentuk elektronik',
            ]
        }
    }
    return render(request, 'enorma/menu-layanan.html', context)

@login_required
def enorma(request):
    tahun_daftar = request.user.npwp.tgl_daftar.year

    try:
        riwayat_nppn = RiwayatNPPN.objects.filter(npwp=request.user.npwp.npwp)
        nppn_terakhir = riwayat_nppn[0]
    except:
        riwayat_nppn = None
        nppn_terakhir = None

    context = {
        'riwayat_nppn': riwayat_nppn,
        'nppn_terakhir': nppn_terakhir,
        'petunjuk': {
            'list_petunjuk': [
                'Menampilkan daftar pemberitahuan Norma Penghitungan Penghasilan Neto yang telah disampaikan.',
                'Bukti Penerimaan Elekronik (BPE) dapat diunduh ulang dengan tombol Unduh BPE pada kolom Aksi.'
            ]
        }
    }
    
    if sekarang > batas_lapor:
        context['disabled'] = 'disabled'
        messages.add_message(request, messages.WARNING, 'Akses fasilitas NPPN telah ditutup karena batas akhir pemberitahuan NPPN sudah terlewati.')

    if tahun_daftar == tahun_sekarang:
        context['disabled'] = 'disabled'
        messages.add_message(request, messages.WARNING, 'Anda belum dapat menggunakan fasilitas NPPN karena baru terdaftar.')

    return render(request, 'enorma/menu-enorma.html', context)

@login_required
def verifikasi(request):
    # coba ambil NPPN terakhir
    try:
        nppn_terakhir = RiwayatNPPN.objects.filter(npwp=request.user.npwp.npwp).order_by('-tahun_pajak').first().tahun_pajak
    except:
        nppn_terakhir = None
        
    if nppn_terakhir == (tahun_sekarang - 1):
        messages.add_message(request, messages.WARNING, 'NPPN tahun ini sudah pernah dilaporkan.')
        return redirect('enorma')
    else:
        # sediakan form verifikasi
        form = VerifikasiSyarat()

        # ambil data WP
        status_wp = request.user.npwp.id_status_wp
        jenis_wp = request.user.npwp.id_jenis_wp
        klu = request.user.npwp.kode_klu
        klu_norma = DaftarNorma.objects.raw('SELECT DISTINCT id_norma, kode_klu, CASE WHEN norma IS NOT NULL THEN 1 ELSE 0 END AS status_norma FROM public.enorma_daftarnorma WHERE kode_klu = %s', [klu.kode_klu])

        # verifikasi data WP
        if status_wp.id_status_wp in [0, 1]:
            v_status_wp = 1
        else:
            v_status_wp = 0

        if jenis_wp.id_jenis_wp == 11:
            v_jenis_wp = 1
        else:
            v_jenis_wp = 0

        context = {
            'form': form,
            'status_wp': status_wp.ket_status_wp,
            'jenis_wp': jenis_wp.ket_jenis_wp,
            'klu': klu.ket_klu,
            'verifikasi': {
                    'status_wp': v_status_wp,
                    'jenis_wp': v_jenis_wp,
                    'status_klu': klu_norma[0].status_norma
                },
            'petunjuk': {
                'list_petunjuk': [
                    'Verifikasi persyaratan dilakukan terhadap data profil Wajib Pajak yang berhak menggunakan fasilitas NPPN.',
                    'DJP berhak menolak pengajuan fasilitas NPPN di kemudian hari bila ditemukan data Wajib Pajak yang tidak sesuai peraturan.'
                ]
            }
        }

        return render(request, 'enorma/menu-lapor-1.html', context)

@login_required
def lapor(request):
    if request.method == 'POST':
        if (request.POST.get('verifikasi', False)):

            status_wp = request.user.npwp.id_status_wp
            jenis_wp = request.user.npwp.id_jenis_wp
            klu = request.user.npwp.kode_klu
            klu_norma = DaftarNorma.objects.raw('SELECT DISTINCT id_norma, kode_klu, CASE WHEN norma IS NOT NULL THEN 1 ELSE 0 END AS status_norma FROM public.enorma_daftarnorma WHERE kode_klu = %s', [klu.kode_klu])

            if status_wp.id_status_wp not in [0, 1]:
                messages.add_message(request, messages.WARNING, 'Status Anda tidak memenuhi syarat.')

            if jenis_wp.id_jenis_wp != 11:
                messages.add_message(request, messages.WARNING, 'Hanya WP OP yang dapat menggunakan fasilitas NPPN.')

            if klu_norma[0].status_norma != 1:
                messages.add_message(request, messages.WARNING, 'KLU Anda tidak memenuhi syarat.')

            if messages.get_messages(request):
                print(messages.get_messages(request))
                return redirect('verifikasi')

            klp_daerah = request.user.npwp.kode_pos.kategori_klp_daerah.kategori_klp_daerah
            klu = request.user.npwp.kode_klu.kode_klu
            norma = DaftarNorma.objects.raw("""SELECT id_norma, norma
                    FROM public.enorma_daftarnorma
                    WHERE kategori_klp_daerah = %s
                    AND kode_klu = %s""", [klp_daerah, klu])
            spt = SPTTahunan1770.objects.filter(npwp=request.user.npwp.npwp, tahun_pajak=tahun_sekarang - 1).order_by('-pembetulan').first()

            if spt is not None:
                messages.add_message(request, messages.INFO, f'Nilai omset terisi otomatis karena anda telah melaporkan SPT Tahunan tahun pajak {tahun_sekarang - 1}.')

            context = {
                'tahun_pajak': tahun_sekarang - 1,
                'norma': norma[0].norma,
                'spt': spt,
                'petunjuk': {
                    'list_petunjuk': [
                        'Verifikasi persyaratan dilakukan terhadap data profil Wajib Pajak yang berhak menggunakan fasilitas NPPN.',
                        'DJP dapat menolak pengajuan fasilitas NPPN di kemudian hari bila ditemukan data Wajib Pajak yang tidak sesuai peraturan.'
                    ]
                }
            }

            return render(request, 'enorma/menu-lapor-2.html', context)
        
        else:
            messages.add_message(request, messages.WARNING, 'Harus melewati proses verifikasi persyaratan terlebih dahulu.')
            return redirect('verifikasi')
    
    else:
        messages.add_message(request, messages.WARNING, 'Harus melewati proses verifikasi persyaratan terlebih dahulu.')
        return redirect('verifikasi')

@login_required
@require_http_methods(["POST"])
def kirim_laporan(request):
    klp_daerah = request.user.npwp.kode_pos.kategori_klp_daerah.kategori_klp_daerah
    klu = request.user.npwp.kode_klu.kode_klu
    norma = DaftarNorma.objects.raw("""SELECT id_norma, norma
            FROM public.enorma_daftarnorma
            WHERE kategori_klp_daerah = %s
            AND kode_klu = %s""", [klp_daerah, klu])

    if 0 < int(request.POST['omset']) <= 4800000000:
        try:
            # menyimpan NPPN yang dikirim
            nppn = RiwayatNPPN(
                npwp = MasterfileWajibPajak.objects.get(npwp=request.user.npwp.npwp),
                tahun_pajak = tahun_sekarang - 1,
                omset = request.POST['omset'],
                id_norma = DaftarNorma.objects.get(id_norma=norma[0].id_norma),
                penghasilan_neto = float(request.POST['omset']) * float(norma[0].norma / 100)
            )

            nppn.save()

            # menambahkan flag penggunaan NPPN pada akun Wajib Pajak
            wp = AkunWajibPajak(
                npwp = MasterfileWajibPajak.objects.get(npwp=request.user.npwp.npwp),
                fg_nppn = 1
            )

            wp.save(update_fields=['fg_nppn'])

        except IntegrityError:
            messages.add_message(request, messages.WARNING, 'Fasilitas NPPN hanya dapat digunakan sekali dalam setahun.')
            return redirect('enorma')
    
    else:
        messages.add_message(request, messages.WARNING, 'Mohon isi omset dengan benar, omset tidak boleh 0 atau melebihi 4,8 M.')
        return redirect('enorma')

    messages.add_message(request, messages.INFO, 'NPPN berhasil dikirim, tanda terima akan diunduh secara otomatis.')
    return redirect('enorma')

def download_pdf(request, pk):
    # ambil data NPPN
    wp = request.user.npwp
    nppn = get_object_or_404(RiwayatNPPN, pk=pk)
    if nppn.npwp != wp:
        messages.add_message(request, messages.WARNING, 'Anda tidak berhak mengakses data ini.')
        return render(request, 'enorma/menu-enorma.html')

    # render dari HTML dan CSS ke PDF
    html_string = render_to_string('enorma/print/tanda-terima.html', {'wp': wp, 'nppn': nppn})
    html = HTML(string=html_string, base_url=request.build_absolute_uri())
    result = html.write_pdf(stylesheets=[CSS(string=render_to_string('enorma/print/print.css'))])

    # bikin response
    response = HttpResponse(result, content_type='application/pdf;')
    response['Content-Disposition'] = 'attachment; filename=tanda_terima_nppn.pdf'

    return response

@api_view(['GET'])
@permission_classes([HasAPIKey])
def api_pengawasan(request, npwp):

    wp = get_object_or_404(MasterfileWajibPajak.objects, pk=npwp)
    nppn = RiwayatNPPNSerializer(RiwayatNPPN.objects.filter(npwp=npwp), many=True)
    spt = SPTTahunan1770Serializer(SPTTahunan1770.objects.filter(npwp=npwp), many=True)

    api = {
        'wp': {
            'npwp': wp.npwp,
            'nama': wp.nama,
            'kode_klu': wp.kode_klu.kode_klu,
            'ket_klu': wp.kode_klu.ket_klu
        },
        'nppn': nppn.data,
        'spt': spt.data
    }
    return Response(api)

@api_view(['POST'])
@permission_classes([HasAPIKey])
def api_penolakan(request, pk):

    nppn = get_object_or_404(RiwayatNPPN.objects, pk=pk)
    serializer = RiwayatNPPNSerializerAll(nppn, data=request.data, partial=True)
    if serializer.is_valid(raise_exception=True):
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK )
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def error_404(request, exception):
    return render(request, 'enorma/404.html', status=404)