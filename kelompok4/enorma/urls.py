from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.wp_login, name="login"),
    path('', views.home, name="home"),
    path('layanan/', views.layanan, name="layanan"),
    path('enorma/', views.enorma, name="enorma"),
    path('enorma/verifikasi/', views.verifikasi, name="verifikasi"),
    path('enorma/lapor/', views.lapor, name="lapor"),
    path('enorma/kirim-laporan/', views.kirim_laporan, name="kirim-laporan"),
    path('enorma/download/<int:pk>', views.download_pdf, name="download"),
    path('enorma/api/pengawasan/<str:npwp>', views.api_pengawasan, name="api-pengawasan"),
    path('enorma/api/pengawasan/<int:pk>/tolak/', views.api_penolakan, name="api-penolakan"),
]
