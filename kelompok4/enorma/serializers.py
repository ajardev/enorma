from rest_framework import serializers
from .models import RiwayatNPPN, SPTTahunan1770, DaftarNorma

class DaftarNormaSerializer(serializers.ModelSerializer):
    class Meta:
        model = DaftarNorma
        fields = ['no_norma', 'norma']

class RiwayatNPPNSerializer(serializers.ModelSerializer):
    id_norma = DaftarNormaSerializer(read_only=True)

    class Meta:
        model = RiwayatNPPN
        fields = ['id_nppn','tahun_pajak', 'omset', 'id_norma', 'penghasilan_neto', 'tanggal_terima', 'fg_ditolak', 'alasan_ditolak']

class RiwayatNPPNSerializerAll(serializers.ModelSerializer):

    class Meta:
        model = RiwayatNPPN
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.fg_ditolak = validated_data.get('fg_ditolak', instance.fg_ditolak)
        instance.alasan_ditolak = validated_data.get('alasan_ditolak', instance.alasan_ditolak)
        instance.save()
        return instance

class SPTTahunan1770Serializer(serializers.ModelSerializer):
    class Meta:
        model = SPTTahunan1770
        fields = ['tahun_pajak', 'pembetulan', 'fg_pembukuan', 'omset', 'norma', 'penghasilan_neto', 'tanggal_terima']