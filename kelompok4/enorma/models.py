from datetime import date
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser

class ActualCharField(models.CharField):
    def db_type(self, connection):
        varchar: str = super().db_type(connection)
        char: str = varchar.replace('varchar', 'char')
        return char

class RefStatusWP(models.Model):
    id_status_wp = models.SmallIntegerField(primary_key=True)
    ket_status_wp = models.CharField(max_length=255)

class RefJenisWP(models.Model):
    id_jenis_wp = models.SmallIntegerField(primary_key=True)
    ket_jenis_wp = models.CharField(max_length=255)

class RefKLU(models.Model):
    kode_klu = ActualCharField(max_length=5, primary_key=True)
    ket_klu = models.CharField(max_length=255)

class RefProvinsi(models.Model):
    kode_provinsi = models.SmallIntegerField(primary_key=True)
    nama_provinsi = models.CharField(max_length=255)

class RefKabKota(models.Model):
    kode_kab_kota = models.IntegerField(primary_key=True)
    nama_kab_kota = models.CharField(max_length=255)

class RefKlpDaerah(models.Model):
    kategori_klp_daerah = models.SmallIntegerField(primary_key=True)
    deskripsi_klp_daerah = models.CharField(max_length=255)

class RefKPP(models.Model):
    kode_kpp = ActualCharField(max_length=3, primary_key=True)
    nama_kpp = models.CharField(max_length=255)
    alamat = models.CharField(max_length=255)
    no_telp = models.CharField(max_length=255)

class RefKodePos(models.Model):
    kode_pos = ActualCharField(max_length=5, primary_key=True)
    kode_provinsi = models.ForeignKey(RefProvinsi, on_delete=models.PROTECT, db_column='kode_provinsi')
    kode_kab_kota = models.ForeignKey(RefKabKota, on_delete=models.PROTECT, db_column='kode_kab_kota')
    kecamatan = models.CharField(max_length=255)
    kategori_klp_daerah = models.ForeignKey(RefKlpDaerah, on_delete=models.PROTECT, db_column='kategori_klp_daerah')

class MasterfileWajibPajak(models.Model):
    npwp = ActualCharField(max_length=15, primary_key=True)
    nama = models.CharField(max_length=255)
    id_status_wp = models.ForeignKey(RefStatusWP, on_delete=models.PROTECT, db_column='id_status_wp')
    id_jenis_wp = models.ForeignKey(RefJenisWP, on_delete=models.PROTECT, db_column='id_jenis_wp')
    kode_klu = models.ForeignKey(RefKLU, on_delete=models.PROTECT, db_column='kode_klu')
    kode_kpp = models.ForeignKey(RefKPP, on_delete=models.PROTECT, db_column='kode_kpp')
    email = models.CharField(max_length=255)
    tgl_daftar = models.DateField()
    alamat = models.CharField(max_length=255)
    kode_pos = models.ForeignKey(RefKodePos, on_delete=models.PROTECT, db_column='kode_pos')

class WPManager(BaseUserManager):
    def create_user(self, npwp, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not npwp:
            raise ValueError('Wajib punya NPWP')

        user = self.model(
            npwp=MasterfileWajibPajak.objects.get(npwp=npwp),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, npwp, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            npwp,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class AkunWajibPajak(AbstractBaseUser):
    npwp = models.OneToOneField(MasterfileWajibPajak, on_delete=models.CASCADE, primary_key=True, db_column='npwp')
    fg_nppn = models.BooleanField(default=False)

    objects = WPManager()

    USERNAME_FIELD = 'npwp'

class SPTTahunan1770(models.Model):
    id_spt = models.IntegerField(primary_key=True)
    npwp = models.ForeignKey(MasterfileWajibPajak, on_delete=models.CASCADE, db_column='npwp')
    tahun_pajak = models.IntegerField()
    pembetulan = models.SmallIntegerField()
    fg_pembukuan = models.BooleanField(null=True)
    omset = models.DecimalField(max_digits=38, decimal_places=2)
    norma = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    penghasilan_neto = models.DecimalField(max_digits=38, decimal_places=2)
    tanggal_terima = models.DateField()

class DaftarNorma(models.Model):
    id_norma = models.IntegerField(primary_key=True)
    no_norma = models.IntegerField()
    kode_klu = models.ForeignKey(RefKLU, on_delete=models.PROTECT, db_column='kode_klu')
    kategori_klp_daerah = models.ForeignKey(RefKlpDaerah, on_delete=models.PROTECT, db_column='kategori_klp_daerah')
    norma = models.DecimalField(max_digits=5, decimal_places=2, null=True)

class RiwayatNPPN(models.Model):
    id_nppn = models.AutoField(primary_key=True)
    npwp = models.ForeignKey(MasterfileWajibPajak, on_delete=models.CASCADE, db_column='npwp')
    tahun_pajak = models.IntegerField()
    omset = models.DecimalField(max_digits=38, decimal_places=2)
    id_norma = models.ForeignKey(DaftarNorma, on_delete=models.PROTECT, db_column='id_norma')
    penghasilan_neto = models.DecimalField(max_digits=38, decimal_places=2)
    tanggal_terima = models.DateField(default=date.today)
    fg_ditolak = models.BooleanField(default=False)
    alasan_ditolak = models.CharField(max_length=255, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['npwp', 'tahun_pajak'], name='nppn_constraint')
        ]