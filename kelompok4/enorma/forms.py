from django import forms
from enorma.models import MasterfileWajibPajak, RiwayatNPPN
from django.core.exceptions import ValidationError

class OneField(forms.Field):
    def to_python(self, value):
        if not value:
            return []
        return value

    def validate(self, value):
        """Check if value consists only of valid emails."""
        # Use the parent's handling of required fields, etc.
        super().validate(value)
        if value != 1:
            raise ValidationError('Error status WP')

class VerifikasiSyarat(forms.Form):
    verifikasi = forms.BooleanField(widget=forms.HiddenInput(), initial=1, required=False)

    # v_status_wp = OneField()
    # v_jenis_wp = OneField()
    # v_klu_wp = OneField()

    # v_status_wp = forms.IntegerField()
    # v_jenis_wp = forms.IntegerField()
    # v_klu_wp = forms.IntegerField()

    # def cek_syarat(self):
    #     v_status_wp = self.cleaned_data['v_status_wp']
    #     v_jenis_wp = self.cleaned_data['v_jenis_wp']
    #     v_klu_wp = self.cleaned_data['v_klu_wp']

    #     if v_status_wp != 1:
    #         raise ValidationError('Error status WP')
    #     elif v_jenis_wp != 1:
    #         raise ValidationError('Error jenis WP')
    #     elif v_klu_wp != 1:
    #         raise ValidationError('Error KLU WP')

    #     return [v_status_wp, v_jenis_wp, v_klu_wp]

class LaporNPPNForm(forms.Form):
    class Meta:
        model = RiwayatNPPN
        fields = ['tahun_pajak', 'omset', 'penghasilan_neto']