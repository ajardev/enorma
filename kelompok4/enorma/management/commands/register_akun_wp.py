from django.core.management.base import BaseCommand, CommandError
from enorma.models import MasterfileWajibPajak, AkunWajibPajak


class Command(BaseCommand):
    help = 'Mass register akun wajib pajak'

    def handle(self, *args, **options):
        mfwp = MasterfileWajibPajak.objects.all().values_list('npwp', flat=True) 

        for npwp in mfwp:
            # save user
            wp = AkunWajibPajak(
                npwp = MasterfileWajibPajak.objects.get(npwp=npwp)
            )
            wp.set_password('enorma4')
            wp.save()

            self.stdout.write(self.style.SUCCESS(f'Sukses mendaftarkan npwp {npwp}'))