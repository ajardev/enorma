from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from enorma.models import AkunWajibPajak, MasterfileWajibPajak
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string


class Command(BaseCommand):
    help = 'Kirim email pengingat untuk memberitahukan penggunaan NPPN'

    def handle(self, *args, **options):
        tahun_sekarang = datetime.now().year
        queryset_penerima = MasterfileWajibPajak.objects.raw('SELECT npwp, email, nama FROM public.enorma_masterfilewajibpajak WHERE npwp IN (SELECT npwp FROM public.enorma_akunwajibpajak ea WHERE fg_nppn = TRUE)')

        subject = 'Mengingatkan untuk memberitahukan penggunaan NPPN'
        message = f'Segera laporkan pemberitahuan NPPN Anda sebelum 31 Maret {tahun_sekarang}.'
        sender_email = settings.EMAIL_HOST_USER

        for wp in queryset_penerima:
            context = {
                'nama': wp.nama,
                'npwp': wp.npwp,
                'kpp': wp.kode_kpp.nama_kpp,
                'kpp_telp': wp.kode_kpp.no_telp,
                'tahun_sekarang': tahun_sekarang
            }
            html_message = render_to_string('enorma/email/email-pengingat.html', context)
            # print(context)
            # print('----------------------------')
            # print(html_message)
            # print(sender_email)

            send_mail(subject, message, sender_email, [wp.email], fail_silently=False, html_message=html_message)

            self.stdout.write(self.style.SUCCESS(f'Sukses mengirimkan email ke {wp.email}'))